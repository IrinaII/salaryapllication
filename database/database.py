from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from config import DB_USER, DB_HOST, DB_PASS, DB_NAME, DB_PORT

SQLALCHEMY_DATABASE_URL = f"postgresql://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
engine = create_engine(SQLALCHEMY_DATABASE_URL)
new_session = sessionmaker(engine, expire_on_commit=False)


def get_db():
    db = new_session()
    try:
        yield db
    finally:
        db.close()
