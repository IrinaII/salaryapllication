from fastapi_jwt import JwtAccessBearer
from config import SECRET_KEY

access_security = JwtAccessBearer(secret_key=f'{SECRET_KEY}', auto_error=False)