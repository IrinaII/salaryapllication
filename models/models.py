from sqlalchemy import MetaData, Table, Column, Integer, String, Float, DATE
from sqlalchemy.ext.declarative import declarative_base

metadata = MetaData()
Base = declarative_base()

employee = Table(
    "employee",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("login", String(255), nullable=False),
    Column("password", String(255), nullable=False),
    Column("salary", Float, nullable=False),
    Column("raising_date", DATE, nullable=False),
)


class EmployeeModel(Base):
    __tablename__ = "employee"

    id = Column(Integer, primary_key=True)
    login = Column(String(255), nullable=False)
    password = Column(String(255), nullable=False)
    salary = Column(Float, nullable=False)
    raising_date = Column(DATE, nullable=False)
