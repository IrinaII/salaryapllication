from datetime import date
from typing import Optional

from pydantic import BaseModel


class EmployeeScheme(BaseModel):
    id: int
    login: str
    password: str
    salary: float
    raising_date: date

    class Config:
        orm_mode = True


class Token(BaseModel):
    access_token: Optional[str | None]
