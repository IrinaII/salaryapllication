from typing import Annotated

from fastapi import Depends, HTTPException, Security, status
from datetime import timedelta

from fastapi_jwt import JwtAuthorizationCredentials
from sqlalchemy.orm import Session

from auth.auth import access_security
from database.database import get_db
from models.models import EmployeeModel
from schemas.schemas import EmployeeScheme


def get_employee(login: str, password: str, db: Annotated[Session, Depends(get_db)]) -> EmployeeScheme:
    return db.query(EmployeeModel).filter(EmployeeModel.login == login, EmployeeModel.password == password).first()


def login_employee(login: str, password: str, db: Annotated[Session, Depends(get_db)]) -> str:
    employee_in_db = get_employee(login, password, db)
    if not employee_in_db:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect login or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    result = {'id': employee_in_db.id, 'login': employee_in_db.login,
              'password': employee_in_db.password, 'salary': employee_in_db.salary,
              'raising_date': str(employee_in_db.raising_date)}
    return access_security.create_access_token(subject=result, expires_delta=timedelta(minutes=3))


def get_current_employee(credentials: Annotated[JwtAuthorizationCredentials, Security(access_security)]) -> dict:
    if credentials is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not authenticated",
            headers={"WWW-Authenticate": "Bearer"},
        )
    return credentials.subject
