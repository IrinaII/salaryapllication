from fastapi import FastAPI
from routers.routers import router as salary_router

app = FastAPI(title="Salary Application")
app.include_router(salary_router)
